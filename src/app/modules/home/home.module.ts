import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home.component';

const routes: Routes = [
    {
        path: 'home',
        children: [
            { path: '', component: HomeComponent }
        ]
    }
];

@NgModule({
    declarations: [
        HomeComponent
    ],
    imports: [
        RouterModule.forChild(routes),
    ]
})
export class HomeModule {}
